<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Make default Route when user open application

Route::get('/','ArtistController@index');
Route::get('/getCities/{id}','ArtistController@getCities');
Route::post('/upload-artist-image', 'ArtistController@saveThumbnailImage');
Route::post('/save-artist', 'ArtistController@saveArtistRecord');
Route::get('/search','ArtistController@search');
Route::get('/artist-events/{id}','ArtistController@getArtistEvent');
Route::get('get-countries','ArtistController@GetAllCountries');
