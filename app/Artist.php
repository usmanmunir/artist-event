<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artist extends Model
{
//    public $table = "artist";
    protected $fillable = [
        'name', 'facebook_url', 'image'];
    public function events () {
        return $this->hasMany(Event::class,'artist_id','id');
    }



}
