<?php

namespace App\Http\Controllers;

use DB;
use App\Artist;
use App\Event;
use Illuminate\Http\Request;

class ArtistController extends Controller
{
    //function for load the main template file

    public function index()
    {
        $countries = DB::table("countries")->get();
        return view('artist/add-artist')->with('countries', $countries);
    }

//    function for get the cities and display on cities dropdown list in add artist form
    public function getCities($id)
    {
        $cities = DB::table("cities")
            ->where("country_id", $id)
            ->pluck("name", "id");
        return response()->json($cities);
    }

//    function for the Save artist Record

    public function saveArtistRecord(Request $request)
    {
//        dd($request->all());
        $artistObj = new artist();
        $artistObj->name = $request->get('artistname');
        $artistObj->facebook_url = $request->get('fburl');
        $artistObj->image = $request->get('artist_image');
        $artistRecordSave = $artistObj->save();
        $artistInsertID = $artistObj->id;
        $this->saveArtistEvents($artistInsertID, $request);
        if ($artistRecordSave) {
            return response()->json([
                'data' => $artistRecordSave,
                'message' => 'Artist has been Saved!',
                'alert-type' => 'success'
            ], 200);
        } else {
            return response()->json([
                'message' => 'Something went Wrong',
                'alert-type' => 'error'
            ], 403);
        }
    }
//Function for the save Artist Events
    public function saveArtistEvents($artistId, $eventsRecord)
    {
        $data = $eventsRecord->get('artist');
//dd($data);
        foreach ($data as $key => $value) {
//            dd($value);
            $eventsSave = Event::create(array(
                'artist_id' => $artistId,
                'venu' => $value['venue'],
                'country_id' => $value['country'],
                'city_id' => $value['city'],
                'date' => $value['date'],
            ));
        }
        return $eventsSave;
    }
//Function for QUick Search
    public function search(Request $request)
    {
        if ($request->ajax()) {
            $output = "";
            $artists = DB::table('artists')->where('name', 'LIKE', '%' . $request->search . "%")->get();
            if (empty($request->search)) {
                $artists = '';
            }
            if ($artists) {
                $totalRecord = count($artists);
                $output .= '<p>' . $totalRecord . ' Records Found for ' . $request->search . '</p>';
                foreach ($artists as $key => $artist) {

                    $output .= '<div class="col-sm-6 col-md-4 col-lg-4 col-xs-12"><div class="card card-body"><div class="col-sm-3 col-md-3 col-lg-3 col-xs-3">' .
                        '<img src="' . $artist->image . '" class="rounded-circle"/></div>' .
                        '<div class="col-sm-9 col-md-9 col-xs-9 col-lg-9"><h5 class="card-title"><a href="/artist-events/' . $artist->id . '">' . $artist->name . '</a></h5>' .
                        '<p class="card-text">' . $artist->facebook_url . '</p></div>' .
                        '</div></div>';
                }
                return Response($output);
            }
        }
    }

//    Function for display artist events
    public function getArtistEvent($artistId)
    {
        $artistRecord = Artist::where('id', '=', $artistId)->get();
        $artistEvents = Event::with('artist')
            ->with('country')
            ->with('city')
            ->where('artist_id', '=', $artistId)->get();
        return view('artist/events-list')->with('artistEvents', $artistEvents)->with('artistRecord', $artistRecord);
    }

    public function GetAllCountries()
    {
        $countries = DB::table("countries")->get();
        echo json_encode(array('data' => $countries));
    }
//Function for upload Artist Image
    public function saveThumbnailImage(Request $request)
    {
        $thumbnailImage = $request->file('file');
        $thumbnailImageName = time();
        $folder = '/uploads/artist/artist-images/';
        $imagePath = $folder . $thumbnailImageName . '.' . $thumbnailImage->getClientOriginalExtension();
        $thumbnailImage->move(public_path() . '/uploads/artist/artist-images/', $thumbnailImageName . '.' . $thumbnailImage->getClientOriginalExtension());
        return $imagePath;
    }
}
