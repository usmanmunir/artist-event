<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //
//    public $table = "event";
    protected $fillable = [
        'artist_id', 'venu', 'city_id', 'country_id', 'date'];

    public function artist()
    {
        return $this->belongsTo(Artist::class);
    }

    public function country()
    {
        return $this->hasOne(Country::class, 'id', 'country_id');
    }

    public function city()
    {
        return $this->hasOne(City::class, 'id', 'city_id');
    }


}
