<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 1/4/20
 * Time: 12:33 PM
 */
?>

<script>
    $(function () {
        var date = new Date();
        date.setDate(date.getDate() - 1);
        $("#datepicker").datepicker({
            format: 'yyyy/mm/dd',
            autoclose: true,
            todayHighlight: true,
            startDate: date
        }).datepicker('update', new Date());
    });

    //    Get Cities againt specific select country from dropdown list
    $('.countrydropdown').change(function () {
        var cid = $(this).val();
        if (cid) {
            $.ajax({
                type: "get",
                url: "/getCities/" + cid,
                success: function (res) {
                    if (res) {
                        $("#cities").empty();
                        $("#cities").append('<option>Select city</option>');
                        $.each(res, function (key, value) {
                            $("#cities").append('<option value="' + key + '">' + value + '</option>');
                        });
                    }
                }

            });
        }
    });
    $("#artistFrom").submit(function (event) {
        $('[name*="artist"]').each(function () {
            $(this).rules('add', {
                required: true,
                messages: {
                    required: "Field is Required"
                }
            });
        });
        event.preventDefault();
    });
    $('#artistFrom').validate({
        ignore: [],
        rules: {
            artistname: {
                minlength: 3,
                maxlength: 15,
                required: true
            },
            fburl: {
                minlength: 3,
                url: true,
                required: true
            },
            image: {
                required: true
            },
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form, e) {

            e.preventDefault();
            var fd = $('#artistFrom').serialize()
            console.log(fd);
            $.ajax({
                type: "post",
                url: "{{action('ArtistController@saveArtistRecord') }}",
                data: fd,
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    console.log(data);
                    toastr.success(data.message);
                    $('#myModal').modal('hide');
                    $("#artistFrom").trigger("reset");

                },
                error: function (data) {
                    console.log(data);
                    toastr.error("Something went wrong");
                }
            });
        }

    });
    $('#search').on('keyup', function () {
        $value = $(this).val();
        $.ajax({
            type: 'get',
            url: '{{URL::to('search')}}',
            data: {'search': $value},
            success: function (data) {
                // console.log(data);
                $('.searchresult').html(data);
            }
        });
    })

    $(document).ready(function () {
        var maxField = 10; //Input fields increment limitation
        var x = 1;
        var addButton = $('.addmoreEvent'); //Add button selector
        var wrapper = $('.artistfields'); //Input field wrapper
        var fieldHTML = '<div><div class="form-group row">\n' +
            '                                <label for="inputPassword" class="col-sm-4 col-form-label">Event Venue</label>\n' +
            '                                <div class="col-sm-8">\n' +
            '                                    <input type="text" class="form-control" name="artist[' + x + '][venue]" id="venue">\n' +
            '                                </div>\n' +
            '                            </div><div class="form-group row">\n' +
            '                                <label for="exampleFormControlSelect1" class="col-sm-4 col-form-label">Example\n' +
            '                                    select</label>\n' +
            '                                <div class="col-sm-8">\n' +
            '                                    <select class="form-control countrydropdown" id="dycountry" name="artist[' + x + '][country]">\n' +
            '                                        <option value="">Select Country</option></select></div></div><div class="form-group row">\n' +
            '                                <label for="exampleFormControlSelect1" class="col-sm-4 col-form-label">Example\n' +
            '                                    select</label>\n' +
            '                                <div class="col-sm-8">\n' +
            '                                    <select class="form-control" id="dycities" name="artist[' + x + '][city]">\n' +
            '                                    </select>\n' +
            '                                </div>\n' +
            '                            </div> <div class="form-group row">\n' +
            '                                <label class="col-sm-4">Select Date: </label>\n' +
            '                                <div class="col-sm-8">\n' +
            '                                    <div id="dydatepicker" class="input-group date" data-date-format="mm-dd-yyyy">\n' +
            '                                        <input class="form-control" type="text" name="artist[' + x + '][date]"/>\n' +
            '                                        <span class="input-group-addon"><i\n' +
            '                                                    class="glyphicon glyphicon-calendar"></i></span>\n' +
            '                                    </div>\n' +
            '                                </div>\n' +
            '                            </div><a href="javascript:void(0);" class="remove_button"> <button type="button" class="btn btn-default">Remove Event</button> </a></div></div>'; //New input field html
        //Initial field counter is 1
        //Once add button is clicked
        $(addButton).click(function () {
            //Check maximum number of input fields
            if (x < maxField) {
                $(wrapper).append(fieldHTML); //Add field html
                $('#dycountry').attr('id', 'country' + x);
                $('#dycities').attr('id', 'cities' + x);
                $('#dydatepicker').attr('id', 'datepicker' + x);
                var date = new Date();
                date.setDate(date.getDate() - 1);
                var countryId = x;
                $.getJSON("/get-countries", function (result) {
                    console.log(result.data);
                    $.each(result.data, function (key, value) {
                        // console.log(x);
                        $("#country" + countryId).append(
                            "<option value=" + value.id + ">" + value.name + "</option>"
                        )//name is database field name such as id,name,address etc
                    });
                });
                $("#country" + countryId).change(function () {
                    var cid = $(this).val();
                    if (cid) {
                        $.ajax({
                            type: "get",
                            url: "/getCities/" + cid,
                            success: function (res) {
                                if (res) {
                                    $("#cities" + countryId).empty();
                                    $("#cities" + countryId).append('<option>Select city</option>');
                                    $.each(res, function (key, value) {
                                        $("#cities" + countryId).append('<option value="' + key + '">' + value + '</option>');
                                    });
                                }
                            }

                        });
                    }
                });
                $("#datepicker" + countryId).datepicker({
                    format: 'yyyy/mm/dd',
                    autoclose: true,
                    todayHighlight: true,
                    startDate: date
                }).datepicker('update', new Date());
                // alert('before'+x);
                x++; //Increment field counter
                // alert('after'+x);
            }
        });

        //Once remove button is clicked
        $(wrapper).on('click', '.remove_button', function (e) {
            e.preventDefault();
            $(this).parent('div').remove(); //Remove field html
            x--; //Decrement field counter
        });
    })

</script>
