<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 1/5/20
 * Time: 2:20 PM
 */
?>
@extends('layouts.app')
@section('main_container')

    <div class="container events-container">
        <h5 onclick="goBack()"> <i class="fas fa-angle-left"></i> Back to results</h5>
        <div class="row">

            <div class="col-sm-6">
                @foreach($artistRecord as $artist)
                    <div class="card card-body">
                        <div class="col-sm-3 col-md-3 col-lg-3 col-xs-3">
                            <img src="{{$artist->image}}" class="rounded-circle"/></div>
                        <div class="col-sm-9 col-md-9 col-xs-9 col-lg-9">
                            <h5 class="card-title"><a href="#">{{$artist->name}}</a></h5>
                            <p class="card-text">{{$artist->facebook_url}}</p></div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="row">
            <p class="upcoming-events">{{count($artistEvents)}} upcoming events</p>
            @foreach($artistEvents as $artistEvent)
                <div class="col-sm-4">
                    <div class="events-card card-3">
                        <div class="col-sm-12">
                            <h5 class="event-detail ">Event Detail</h5>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6 col-xs-6">
                            <h5 class="event-info">country</h5>
                            <p>{{$artistEvent->country->name}}</p>

                            <h5 class="event-info">Venu</h5>
                            <p>{{$artistEvent->venu}}</p>
                        </div>
                        <div class="col-sm-6 col-md-6 col-xs-6 col-lg-6">
                            <h5 class="event-info">city</h5>
                            <p>{{$artistEvent->city->name}}</p>

                            <h5 class="event-info">Date</h5>
                            <p>{{$artistEvent->Date}}</p>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>


    </div>


@endsection
@push('styles')
    <style>
        .events-card {
            border-radius: 2px;
            height: 200px;
            margin: 1rem;
            position: relative;
            flex-direction: column;
            word-wrap: break-word;
            background-color: #fff;
            background-clip: border-box;
            border: 0 solid #f7f7f7;
            padding: 4px 0px 0px 0px;
        }

        .card-3 {
            box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
        }
    </style>
    @include('artist.artist-css')
@endpush
@push('scripts')
    <script>
        function goBack() {
            window.history.back();
        }
    </script>

    @include('artist.artist-js')
@endpush
