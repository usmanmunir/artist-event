<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 1/4/20
 * Time: 12:32 PM
 */
?>
<style>
    body{
        background-color: f7f7f7;
    }
.card{
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 0 solid #f7f7f7;
    border-radius: .25rem;
    position: relative;
    padding: 4px 0px 0px 0px;
    height: 110px;
    margin-bottom: 15px;
    box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
}
.card-body img{
    width: 70px;
    height: 80px;
    padding-top: 10px;
    border-radius: 50%;
}
    .modal-button{
        padding-bottom: 5rem;
    }
    #custom-search-input{
        padding: 3px;
        border: solid 1px #E4E4E4;
        border-radius: 6px;
        background-color: #fff;
    }

    #custom-search-input input{
        border: 0;
        box-shadow: none;
    }

    #custom-search-input button{
        margin: 2px 0 0 0;
        background: none;
        box-shadow: none;
        border: 0;
        color: #666666;
        padding: 0 8px 0 10px;
        border-left: solid 1px #ccc;
    }

    #custom-search-input button:hover{
        border: 0;
        box-shadow: none;
        border-left: solid 1px #ccc;
    }

    #custom-search-input .glyphicon-search{
        font-size: 23px;
    }
    .searchform{
        padding-top: 8rem;
    }

    /*Date picker celender pointer*/
    #datepicker > span:hover{cursor: pointer;}

    .container.related-results{
        padding-top: 10rem;
    }
    .event-detail{
        height: auto;
        background-color: #FFF;
    }
    .events-container{
        padding: 25px;
    }
    .event-detail{
        font-weight: 700;
        color: #9c9a9a;
        border-bottom: 1px solid black;
        padding-bottom: 7px;
    }
    .event-info{
        font-weight: 700;
        color: #9c9a9a;
    }
    .upcoming-events{
        padding-left: 20px;
    }
</style>
