<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 1/4/20
 * Time: 2:53 AM
 */
?>
<!--Include app.php file-->
@extends('layouts.app')
@section('main_container')
    <div class="container searchform">
        <div class="row">
            <div class="col-sm-offset-9 col-xs-offset-6 col-md-3 modal-button">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Add Artist
                </button>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-offset-2 col-md-6">
                <div id="custom-search-input">
                    <div class="input-group col-md-12">
                        <input type="text" class="form-control input-lg" id="search" name="search"
                               placeholder="Search Artist"/>
                        <span class="input-group-btn">
                        <button class="btn btn-primary" type="button">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container related-results">
        <div class="row searchresult">
        </div>
    </div>
    {{--Artist Form--}}
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Artist</h4>
                </div>
                <form id="artistFrom" action="#">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-4 col-form-label">Artist Name</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="artistname" id="artistname">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-4 col-form-label">Artist Picture</label>
                            <div class="col-sm-8">
                                <div class="dropzone" id="artistImage">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-4 col-form-label">Artist Facebook Url</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="fburl" id="fburl">
                            </div>
                        </div>

                        <div class="modal-header">
                            <h4 class="modal-title text-center">Add Artist Event Information</h4>
                        </div>
                        <div class="modal-body artistfields">
                            <div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-4 col-form-label">Event Venue</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="artist[0][venue]" id="venue">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleFormControlSelect1" class="col-sm-4 col-form-label">Example
                                        select</label>
                                    <div class="col-sm-8">
                                        <select class="form-control countrydropdown" id="country"
                                                name="artist[0][country]">
                                            <option value="">Select Country</option>
                                            @foreach ($countries as $country)
                                                <option value="{{$country->id}}">
                                                    {{$country->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleFormControlSelect1" class="col-sm-4 col-form-label">Example
                                        select</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" id="cities" name="artist[0][city]">
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4">Select Date: </label>
                                    <div class="col-sm-8">
                                        <div id="datepicker" class="input-group date" data-date-format="mm-dd-yyyy">
                                            <input class="form-control datepicker" type="text" name="artist[0][date]"/>
                                            <span class="input-group-addon"><i
                                                        class="glyphicon glyphicon-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save Artist</button>
                        <button type="button" class="btn btn-primary addmoreEvent">Add More Event</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
@endsection
@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
    <style>
        .dropzone {
            background: white;
            border-radius: 5px;
            border: 2px dashed rgb(0, 135, 247);
            border-image: none;
            max-width: 500px;
            margin-left: auto;
            margin-right: auto;
        }

    </style>
    @include('artist.artist-css')
@endpush
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
    <script type="text/javascript">
        Dropzone.autoDiscover = false;
        var uploadedDocumentMap = {}
        $(document).ready(function () {
            $("#artistImage").dropzone
            ({
                maxFiles: 1,
                timeout: 50000,
                maxFilesize: 8,
                addRemoveLinks: true,
                acceptedFiles: '.jpg,.jpeg,.JPEG,.JPG,.png,.PNG',
                method: "POST",
                options: {
                    tooltip: false
                },
                url: "{{ url('/upload-artist-image') }}",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (file, response) {
                    console.log(response);
                    $('form').append('<input type="hidden" name="artist_image" value="' + response + '">')
                    uploadedDocumentMap[file.path] = response
                }
            });
        })
    </script>
    @include('artist.artist-js')
@endpush